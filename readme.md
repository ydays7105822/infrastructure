# Infrastructure Configuration

This repository contains the Kubernetes and monitoring configurations for deploying and managing the application's infrastructure. The structure of the repository is organized into specific folders for MongoDB, monitoring, and namespaces, with corresponding YAML files for deployments, services, and configurations. Additionally, this repository references backend and frontend services, which are critical components of the overall system.

## Table of Contents

- [Contents](#contents)
  - [k8s/mongo](#k8smongo)
  - [k8s/monitoring](#k8smonitoring)
  - [k8s/namespaces](#k8snamespaces)
  - [scripts](#scripts)
  - [backend](#backend)
  - [frontend](#frontend)
- [Installation](#installation)

## Contents

### k8s

#### mongo
- **deployment.yaml**: Contains the Kubernetes deployment configuration for MongoDB. It defines the MongoDB deployment, specifying the number of replicas, container images, and other deployment settings.
- **pvc.yaml**: Defines the Persistent Volume Claim (PVC) for MongoDB. It requests persistent storage resources from Kubernetes to ensure data persistence across pod restarts.
- **service.yaml**: Contains the Kubernetes service configuration for MongoDB. It exposes the MongoDB deployment internally within the cluster, allowing other services to communicate with it.

#### monitoring

##### Exporters
- **mongo-exporter.yml**: Configures the MongoDB exporter for Prometheus. It allows Prometheus to scrape metrics from MongoDB for monitoring purposes.
- **nginx-exporter.yml**: Configures the NGINX exporter for Prometheus. It allows Prometheus to scrape metrics from NGINX for monitoring purposes.

##### Monitoring Tools
- **grafana.yml**: Contains the configuration for deploying Grafana, a data visualization and monitoring tool. It defines the Grafana deployment and service settings.
- **prometheus.yml**: Contains the configuration for deploying Prometheus, a monitoring and alerting toolkit. It defines the Prometheus deployment, service, and scrape configurations.
- **prometheus.yml.save**: A backup of the Prometheus configuration file.

##### RBAC (Role-Based Access Control)
- **prometheus-cluster-role-binding.yml**: Defines the ClusterRoleBinding for Prometheus, granting it the necessary permissions to scrape metrics from the cluster.
- **prometheus-cluster-role.yml**: Defines the ClusterRole for Prometheus, specifying the permissions required for monitoring and alerting.
- **prometheus-service-account.yml**: Defines the ServiceAccount for Prometheus, associating it with the necessary RBAC roles and bindings.

### namespaces
- **production-namespace.yaml**: Defines the namespace for the production environment. It isolates resources and configurations within the production namespace, providing better resource management and separation.

### scripts
- **delete-all-infra.sh**: Script to delete all infrastructure components.
- **deploy-all-infra.sh**: Script to deploy all infrastructure components.
- **deploy.sh**: Script to deploy specific Kubernetes resources defined in the repository. It applies the configurations and ensures that the infrastructure components are deployed in the correct order.
- **setup_backend.sh**: Script to set up the backend environment.
- **setup_mongo.sh**: Script to set up the MongoDB environment.
- **setup_script.sh**: General setup script for various components.

### backend
- **Dockerfile**: Defines the Docker image for the backend service.
- **Kubernetes Configuration**:
  - **deployment.yaml**: Manages the deployment settings, including replicas and container specifics.
  - **service.yaml**: Exposes the backend service within the Kubernetes cluster.

### frontend
- **Dockerfile**: Builds the Docker image for the frontend application.
- **Kubernetes Configuration**:
  - **deployment.yaml**: Configures the deployment of the frontend service.
  - **service.yaml**: Manages the exposure of the frontend service within the cluster.

## Installation

To set up the infrastructure, follow these steps:

1. Clone the repository:
    ```sh
    git clone <repository-url>
    cd infra
    ```

2. Apply the Kubernetes configurations:
    ```sh
    kubectl apply -f k8s/namespaces/production-namespace.yaml
    kubectl apply -f k8s/mongo/
    kubectl apply -f k8s/monitoring/
    ```

3. Deploy the infrastructure:
    ```sh
    ./scripts/deploy.sh
    ```

This concludes the setup for the infrastructure configuration.
